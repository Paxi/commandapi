package de.paxii.bukkit.commandapi;

import de.paxii.bukkit.commandapi.chat.Chat;
import de.paxii.bukkit.commandapi.command.CommandApiHandler;
import de.paxii.bukkit.commandapi.command.CommandInterface;
import de.paxii.bukkit.commandapi.command.CommandManager;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;

public class CommandApi {

	@Getter
	private String chatPrefix = "[CommandApi] ";
	@Getter
	private CommandManager commandManager;
	@Getter
	private Chat chat;

	public CommandApi() {
		this.commandManager = new CommandManager(this);
		this.chat = new Chat(this);
	}

	public void addCommand(CommandInterface command) {
		this.commandManager.addCommand(command);
	}

	public CommandExecutor getExecutor() {
		return new CommandApiHandler(this);
	}

	public void setChatPrefix(String prefix) {
		if (!ChatColor.stripColor(prefix).endsWith(" ")) {
			prefix += " ";
		}

		this.chatPrefix = prefix;
	}

}
