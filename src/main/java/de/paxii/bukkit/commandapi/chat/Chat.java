package de.paxii.bukkit.commandapi.chat;

import de.paxii.bukkit.commandapi.CommandApi;
import org.bukkit.command.CommandSender;

/**
 * Created by Lars on 06.11.2016.
 */
public class Chat {

	private CommandApi commandApi;

	public Chat(CommandApi commandApi) {
		this.commandApi = commandApi;
	}

	public void sendMessage(CommandSender commandSender, String message) {
		commandSender.sendMessage(this.commandApi.getChatPrefix() + message);
	}

}
