package de.paxii.bukkit.commandapi.command;

import org.bukkit.command.CommandSender;

public interface CommandInterface {

	/**
	 * Name of the Command
	 *
	 * @return String
	 */
	String getCommand();

	/**
	 * List of alias Commands
	 *
	 * @return String[]
	 */
	String[] getAliases();

	/**
	 * List of main Commands
	 *
	 * @return String[]
	 */
	String[] getRootCommands();

	/**
	 * Help for the Command
	 *
	 * @return String
	 */
	String getHelp();

	/**
	 * Syntax of the Command
	 *
	 * @return String
	 */
	String getSyntax();

	/**
	 * Permission needed to execute this Command
	 *
	 * @return String
	 */
	String getPermission();

	/**
	 * Is this command supposed to be executed by players only?
	 *
	 * @return boolean
	 */
	boolean isPlayerOnly();

	/**
	 * Is this command supposed to be executed by console only?
	 *
	 * @return boolean
	 */
	boolean isConsoleOnly();

	/**
	 * Executes this Command
	 *
	 * @param commandSender
	 * @param arguments
	 * @return boolean
	 */
	boolean executeCommand(CommandSender commandSender, String[] arguments);

}
