package de.paxii.bukkit.commandapi.command;

import de.paxii.bukkit.commandapi.CommandApi;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class CommandApiHandler implements CommandExecutor {

	private CommandApi commandApi;
	private CommandManager commandManager;

	public CommandApiHandler(CommandApi commandApi) {
		this.commandApi = commandApi;
		this.commandManager = commandApi.getCommandManager();
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command cmd, String label, String[] args) {
		String chatPrefix = this.commandApi.getChatPrefix();
		if (!chatPrefix.endsWith(" ")) {
			chatPrefix += " ";
		}

		if (args.length > 0) {
			String subCommand = args[0];

			if (this.commandManager.doesCommandExist(subCommand)) {
				CommandInterface command = this.commandManager.getCommandByName(subCommand);
				ArrayList<String> newArgs = new ArrayList<>();
				for (int i = 1; i < args.length; i++) {
					newArgs.add(args[i]);
				}

				if (command.isPlayerOnly() && !(commandSender instanceof Player)) {
					this.sendChatMessage(commandSender, "Dieser Befehl ist nur für Spieler.");
				}

				if (command.isConsoleOnly() && !(commandSender instanceof ConsoleCommandSender)) {
					this.sendChatMessage(commandSender, "Dieser Befehl ist nur für die Console gedacht.");
				}

				if (commandSender.hasPermission(command.getPermission())) {
					if (!command.executeCommand(commandSender, newArgs.toArray(new String[newArgs.size()]))) {
						this.sendChatMessage(commandSender, "Fehlerhafte Eingabe! (\"/" + command.getRootCommands()[0] + " " + command.getSyntax() + "\")");
					}
				} else {
					this.sendChatMessage(commandSender, "Du hast keine Berechtigung für diesen Befehl!");
				}
			} else {
				this.sendChatMessage(commandSender, "Dieser Befehl existiert nicht!");
			}
		} else {
			if (this.commandManager.doesCommandExist("help")) {
				return this.commandManager.getCommandByName("help").executeCommand(commandSender, args);
			} else {
				this.sendChatMessage(commandSender, "Verfügbare Befehle:");
				this.commandManager.getCommandList().forEach(commandInterface -> {
					if (commandSender.hasPermission(commandInterface.getPermission())) {
						this.sendChatMessage(commandSender,
										String.format("%s/%s %s:",
														ChatColor.UNDERLINE,
														commandInterface.getRootCommands()[0],
														commandInterface.getCommand()
										)
						);
						this.sendChatMessage(commandSender, commandInterface.getHelp());
					}
				});
			}
		}

		return true;
	}

	private void sendChatMessage(CommandSender commandSender, String message) {
		this.commandApi.getChat().sendMessage(commandSender, message);
	}

}