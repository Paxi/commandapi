package de.paxii.bukkit.commandapi.command;

import de.paxii.bukkit.commandapi.CommandApi;
import lombok.Getter;

import java.util.ArrayList;

public class CommandManager {

	private CommandApi commandApi;

	/**
	 * List of commands
	 */
	@Getter
	private ArrayList<CommandInterface> commandList;

	public CommandManager(CommandApi commandApi) {
		this.commandApi = commandApi;
		this.commandList = new ArrayList<>();
	}

	/**
	 * Add new CommandInterface to the CommandManager
	 *
	 * @param commandInterface
	 */
	public void addCommand(CommandInterface commandInterface) {
		if (!this.commandList.contains(commandInterface)) {
			if (commandInterface instanceof AbstractCommand) {
				AbstractCommand abstractCommand = (AbstractCommand) commandInterface;
				abstractCommand.setCommandApi(this.commandApi);
				this.commandList.add(abstractCommand);
			} else {
				this.commandList.add(commandInterface);
			}
		}
	}

	/**
	 * Get a CommandInterface by Name
	 *
	 * @param subCommand
	 * @return CommandInterface or null if nonexistent
	 */
	public CommandInterface getCommandByName(String subCommand) {
		for (CommandInterface command : this.commandList) {
			if (command.getCommand().equalsIgnoreCase(subCommand)) {
				return command;
			}

			for (String alias : command.getAliases()) {
				if (alias.equalsIgnoreCase(subCommand)) {
					return command;
				}
			}

			for (String rootCommand : command.getRootCommands()) {
				if (rootCommand.equalsIgnoreCase(subCommand)) {
					return command;
				}
			}
		}

		return null;
	}

	/**
	 * Check whether a CommandInterface exists
	 *
	 * @param subCommand
	 * @return
	 */
	public boolean doesCommandExist(String subCommand) {
		for (CommandInterface command : this.commandList) {
			if (command.getCommand().equalsIgnoreCase(subCommand)) {
				return true;
			}

			for (String alias : command.getAliases()) {
				if (alias.equalsIgnoreCase(subCommand)) {
					return true;
				}
			}

			for (String rootCommand : command.getRootCommands()) {
				if (rootCommand.equalsIgnoreCase(subCommand)) {
					return true;
				}
			}
		}

		return false;
	}
}

