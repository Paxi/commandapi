package de.paxii.bukkit.commandapi.command;

import de.paxii.bukkit.commandapi.CommandApi;
import lombok.Setter;
import org.bukkit.command.CommandSender;

/**
 * Created by Lars on 30.10.2016.
 */
public abstract class AbstractCommand implements CommandInterface {

	@Setter
	private CommandApi commandApi;

	@Override
	public String[] getAliases() {
		return new String[0];
	}

	@Override
	public String[] getRootCommands() {
		return new String[] { this.getCommand() };
	}

	@Override
	public boolean isPlayerOnly() {
		return false;
	}

	@Override
	public boolean isConsoleOnly() {
		return false;
	}

	@Override
	public String getSyntax() {
		return this.getCommand();
	}

	protected void sendMessage(CommandSender commandSender, String message) {
		commandSender.sendMessage(this.commandApi.getChatPrefix() + message);
	}

}
